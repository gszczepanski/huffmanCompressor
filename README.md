# Info
This is compressing program for .txt files.
It uses Huffman algorithm for loseless compression.

It can compress file up to 50% size of original file (e.g. <b>408 MB -> 243 MB</b>). 

### <a href="https://github.com/gspansky/huffmanCompressor/releases/download/1.0/huffmanCompressor.zip">Download program</a>

# Usage
Go to folder with hc.jar and type in console:
```
java -jar hc.jar [command] pathToFile
```
where [command] can be encode or decode.

Examples:
```
java -jar hc.jar encode text.txt
java -jar hc.jar decode text.huff
```
Compressed files are saved with <b>.huff</b> extension.
