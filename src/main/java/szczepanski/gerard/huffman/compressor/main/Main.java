package szczepanski.gerard.huffman.compressor.main;

import szczepanski.gerard.huffman.compressor.algorithm.Compressor;
import szczepanski.gerard.huffman.compressor.algorithm.CompressorFactory;

public class Main {


    public static void main(String... args) {
        validateArgs(args);

        String command = args[0];
        String filePath = args[1];

        CompressorFactory factory = new CompressorFactory();
        Compressor compressor = factory.createCompressor();

        compressor.process(command, filePath);
    }

    private static void validateArgs(String... args) {
        if (args.length != 2) {
            throw new RuntimeException("Given arguments not valid! type [encode/decode] [filename]. Eg encode test.txt");
        }
    }

}
