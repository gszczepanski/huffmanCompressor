package szczepanski.gerard.huffman.compressor.algorithm;

public class CompressorFactory {

    public Compressor createCompressor() {
        return new CompressorImpl();
    }

}
