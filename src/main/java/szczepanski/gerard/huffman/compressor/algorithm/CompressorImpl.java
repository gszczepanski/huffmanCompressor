package szczepanski.gerard.huffman.compressor.algorithm;

import org.apache.log4j.Logger;

import java.io.File;

import static szczepanski.gerard.huffman.compressor.algorithm.CommandToken.*;

/**
 * Compressor is the Algorithm facade.
 *
 * @author Gerard Szczepanski
 */
class CompressorImpl implements Compressor {
    private static final Logger LOG = Logger.getLogger(CompressorImpl.class);

    private final Algorithm huffmanAlgorithm = new Algorithm();

    @Override
    public void process(String command, String filePath) {
        CommandToken token = resolveCommand(command);
        File file = new File(filePath);
        runAlgorithm(token, file);
    }

    private CommandToken resolveCommand(String command) {
        String uppercaseCommand = command.toUpperCase();
        try {
            return valueOf(uppercaseCommand);
        } catch (IllegalArgumentException iae) {
            throw new RuntimeException("Invalid command. Use encode/decode to run program!");
        }
    }

    private void runAlgorithm(CommandToken token, File file) {
        long algorithmStartTimestamp = System.currentTimeMillis();
        if (token == ENCODE) {
            processEncoding(file);
        } else {
            processDecoding(file);
        }
        long algorithmEndTimestamp = System.currentTimeMillis();
        LOG.info(String.format("Processed in %d ms", (algorithmEndTimestamp - algorithmStartTimestamp)));
    }

    private void processEncoding(File file) {
        LOG.info("Process Encoding " + file);
        CompressedFile compressedFile = huffmanAlgorithm.encode(file);
        compressedFile.storeOnDisk();
    }

    private void processDecoding(File file) {
        LOG.info("Process Decoding " + file);
        CompressedFile compressedFile = CompressedFile.loadFromDisk(file);
        huffmanAlgorithm.decode(compressedFile);
        compressedFile.removeFromDisk();
    }

}
