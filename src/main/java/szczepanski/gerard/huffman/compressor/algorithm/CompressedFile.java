package szczepanski.gerard.huffman.compressor.algorithm;

import it.unimi.dsi.fastutil.chars.Char2ObjectMap;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.util.BitSet;

/**
 * Represents compressed file stored on disk with .huff extension.
 * This file contains compressed bits set, Map of char bit codes, and original file info.
 * <p>
 * This file is responsible for store/restore itself on disk and also delete itself from disk.
 *
 * @author Gerard Szczepanski
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class CompressedFile implements Serializable {
    private static final long serialVersionUID = -8820626009092433631L;

    private static final String COMPRESSED_FILE_EXTENSION = ".huff";
    private static final String DOT = ".";

    private final String fileName;
    private final String fileExtension;

    @Getter
    private final BitSet compressedFileBits;

    @Getter
    private final Char2ObjectMap<BitSet> bitCodes;

    public static CompressedFile createFromFile(File file, BitSet compressedFileBits, Char2ObjectMap<BitSet> bitCodes) {
        String fileName = FilenameUtils.getBaseName(file.getAbsolutePath());
        String fileExtension = FilenameUtils.getExtension(file.getName());
        return new CompressedFile(fileName, fileExtension, compressedFileBits, bitCodes);
    }

    public static CompressedFile loadFromDisk(File file) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            CompressedFile compressedFile = (CompressedFile) ois.readObject();
            ois.close();
            return compressedFile;
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void storeOnDisk() {
        ObjectOutputStream oos;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(getCompressedFile()));
            oos.writeObject(this);
            oos.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void removeFromDisk() {
        File compressedFile = getCompressedFile();
        compressedFile.delete();
    }

    private File getCompressedFile() {
        return new File(fileName + COMPRESSED_FILE_EXTENSION);
    }

    public File getOriginalFile() {
        return new File(fileName + DOT + fileExtension);
    }

}
