package szczepanski.gerard.huffman.compressor.algorithm;

import it.unimi.dsi.fastutil.chars.Char2ObjectMap;
import it.unimi.dsi.fastutil.chars.Char2ObjectOpenHashMap;
import lombok.Getter;
import szczepanski.gerard.huffman.compressor.algorithm.HuffmanTree.Node;
import szczepanski.gerard.huffman.compressor.algorithm.HuffmanTree.ParentNode;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * This visitor extends HuffmanTree behavior.
 * It generates codes for HuffmanTree based on HuffmanTree structure.
 *
 * @author Gerard Szczepanski
 */
class HuffmanVisitor {

    private static final String ROOT_NODE_CODE = "";
    private static final String RIGHT_NODE_CODE = "1";
    private static final String LEFT_NODE_CODE = "0";

    private Map<Character, String> codesMap;

    @Getter
    private Char2ObjectMap<BitSet> bitsMap;

    protected void visit(HuffmanTree tree) {
        codesMap = new HashMap<>();

        Node rootNode = tree.buildTree();
        traverse(rootNode, ROOT_NODE_CODE);
        generateBitCodes();
    }

    private void traverse(Node node, String code) {
        if (node == null) {
            return;
        }

        if (node.isParentNode) {
            ParentNode parentNode = (ParentNode) node;

            traverse(parentNode.left, code + LEFT_NODE_CODE);
            traverse(parentNode.right, code + RIGHT_NODE_CODE);
        } else {
            codesMap.put(node.valueOf, code);
        }
    }

    private void generateBitCodes() {
        bitsMap = new Char2ObjectOpenHashMap<>();
        Set<Entry<Character, String>> codesSet = codesMap.entrySet();

        codesSet.forEach(c -> {
            String code = c.getValue();
            BitSet codeBits = new BitSet(code.length());

            int i;
            for (i = 0; i < code.length(); i++) {
                if (code.charAt(i) == '0') {
                    codeBits.set(i, false);
                } else {
                    codeBits.set(i, true);
                }
            }

            codeBits.set(i, true); // Extra bit represents end of code
            bitsMap.put(c.getKey(), codeBits);
        });
    }
}
