package szczepanski.gerard.huffman.compressor.algorithm;

import it.unimi.dsi.fastutil.chars.Char2ObjectMap;
import it.unimi.dsi.fastutil.chars.Char2ObjectOpenHashMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Implementation of HuffmanTree structure.
 * <p>
 * This class instance allow to add chars, and then construct HuffmanTree.
 * Each char frequency is counted.
 *
 * @author Gerard Szczepanski
 */
class HuffmanTree {

    protected final Char2ObjectMap<Node> nodesMap = new Char2ObjectOpenHashMap<>();
    protected int numberOfChars;
    protected int numberOfNodes;

    public HuffmanTree() {
        numberOfChars = 0;
        numberOfNodes = 0;
    }

    public void addChar(char c) {

        if (nodesMap.containsKey(c)) {
            Node existingNode = nodesMap.get(c);
            existingNode.times++;
        } else {
            nodesMap.put(c, Node.of(c));
            numberOfNodes++;
        }

        numberOfChars++;
    }

    /**
     * Construct Huffman Tree from stored chars and their frequencies.
     * Return root node of the tree.
     */
    protected Node buildTree() {
        List<Node> nodesList = getNodesList();

        if (nodesList.size() != 1) {
            sort(nodesList);

            while (nodesList.size() > 1) {
                Node firstLowestNode = nodesList.get(0);
                Node secondLowestNode = nodesList.get(1);

                Node meltedNode = ParentNode.of(firstLowestNode, secondLowestNode);
                nodesList.set(0, meltedNode);
                nodesList.remove(1);
                sort(nodesList);
            }
        }

        return nodesList.get(0);
    }

    private void sort(List<Node> nodesList) {
        nodesList.sort((n1, n2) -> Integer.compare(n1.times, n2.times));
    }

    private List<Node> getNodesList() {
        List<Node> nodesList = new ArrayList<>(nodesMap.size());

        Set<Entry<Character, Node>> nodesEntrySet = nodesMap.entrySet();
        for (Entry<Character, Node> nodeEntry : nodesEntrySet) {
            nodesList.add(nodeEntry.getValue());
        }

        return nodesList;
    }

    static class Node {

        protected boolean isParentNode = false;
        protected char valueOf;
        protected int times;

        protected static Node of(char c) {
            Node node = new Node();
            node.valueOf = c;
            node.times = 1;
            return node;
        }

        protected static Node parentNode(int times) {
            Node node = new Node();
            node.times = times;
            node.isParentNode = true;
            return node;
        }

        @Override
        public String toString() {
            return "(" + valueOf + ":" + times + ")";
        }
    }

    static class ParentNode extends Node {

        protected Node left;
        protected Node right;

        protected static ParentNode of(Node left, Node right) {
            ParentNode node = new ParentNode();

            node.isParentNode = true;
            node.left = left;
            node.right = right;
            node.times = left.times + right.times;

            return node;
        }

    }
}
