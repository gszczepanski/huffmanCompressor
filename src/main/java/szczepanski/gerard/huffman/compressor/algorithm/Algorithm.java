package szczepanski.gerard.huffman.compressor.algorithm;

import it.unimi.dsi.fastutil.chars.Char2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2CharMap;
import it.unimi.dsi.fastutil.objects.Object2CharOpenHashMap;
import szczepanski.gerard.huffman.compressor.util.IOHelper;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.BitSet;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Huffman Algorithm implementation.
 *
 * @author Gerard Szczepanski
 */
class Algorithm {
    private static final int EOF = -1;

    public CompressedFile encode(File file) {
        Char2ObjectMap<BitSet> bitCodes = createBitCodesForChars(file);
        return compressFile(file, bitCodes);
    }

    private Char2ObjectMap<BitSet> createBitCodesForChars(File file) {
        HuffmanTree tree = new HuffmanTree();
        HuffmanVisitor visitor = new HuffmanVisitor();

        fillTree(tree, file);
        visitor.visit(tree);

        return visitor.getBitsMap();
    }

    private void fillTree(HuffmanTree tree, File file) {
        Reader reader = IOHelper.loadInputReader(file);

        int r;
        try {
            while ((r = reader.read()) != EOF) {
                tree.addChar((char) r);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private CompressedFile compressFile(File file, Char2ObjectMap<BitSet> bitCodes) {
        try {
            Reader reader = IOHelper.loadInputReader(file);
            int r;
            BitSet encodedBits = new BitSet();
            int encodedBitsIndex = 0;

            while ((r = reader.read()) != EOF) {
                char c = (char) r;
                BitSet code = bitCodes.get(c);

                for (int i = 0; i < code.length() - 1; i++) {
                    boolean b = code.get(i);
                    encodedBits.set(encodedBitsIndex, b);
                    encodedBitsIndex++;
                }
            }

            reader.close();
            addExtraBitToAvoitBitStreamTrim(encodedBits, encodedBitsIndex);

            return CompressedFile.createFromFile(file, encodedBits, bitCodes);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void addExtraBitToAvoitBitStreamTrim(BitSet encodedBits, int encodedBitsIndex) {
        encodedBits.set(encodedBitsIndex, true);
    }

    public void decode(CompressedFile compressedFile) {
        File originalFile = compressedFile.getOriginalFile();
        BitSet compressedFileBits = compressedFile.getCompressedFileBits();
        Object2CharMap<String> stringCodes = createStringCodesMap(compressedFile.getBitCodes());

        decodeFile(originalFile, compressedFileBits, stringCodes);
    }

    private Object2CharMap<String> createStringCodesMap(Char2ObjectMap<BitSet> bitCodes) {
        Object2CharMap<String> map = new Object2CharOpenHashMap<>();

        Set<Entry<Character, BitSet>> bitCodesEntrySet = bitCodes.entrySet();
        bitCodesEntrySet.forEach(e -> {
            StringBuilder code = new StringBuilder();
            BitSet bitCode = e.getValue();

            for (int i = 0; i < bitCode.length() - 1; i++) {
                code.append(resolveBitToString(bitCode.get(i)));
            }

            map.put(code.toString(), e.getKey());
        });

        return map;
    }

    private char resolveBitToString(boolean bit) {
        return bit ? '1' : '0';
    }

    private void decodeFile(File file, BitSet compressedFileBits, Object2CharMap<String> stringCodes) {
        Writer writer = IOHelper.loadOutputWriter(file);

        StringBuilder readCode = new StringBuilder();
        for (int i = 0; i < compressedFileBits.length() - 1; i++) {

            boolean bit = compressedFileBits.get(i);
            readCode.append(resolveBitToString(bit));
            Character c = stringCodes.get(readCode.toString());

            if (c != null) {
                try {
                    writer.write(c.charValue());
                    readCode = new StringBuilder();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        try {
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
