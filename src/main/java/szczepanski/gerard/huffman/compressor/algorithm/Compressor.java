package szczepanski.gerard.huffman.compressor.algorithm;

/**
 * Compressor API.
 *
 * @author Gerard Szczepanski
 */
public interface Compressor {

    /**
     * Start compressor action
     *
     * @param command  - one of encode/decode . Describes compressor action.
     * @param filePath - filePaht to Compress/decompress.
     */
    void process(String command, String filePath);

}
