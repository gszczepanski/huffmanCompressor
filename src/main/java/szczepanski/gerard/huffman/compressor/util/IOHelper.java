package szczepanski.gerard.huffman.compressor.util;

import java.io.*;
import java.nio.charset.Charset;

public class IOHelper {

    public static Reader loadInputReader(File file) {
        try {
            InputStream in = new FileInputStream(file);
            Reader reader = new InputStreamReader(in, Charset.defaultCharset());
            return new BufferedReader(reader);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static Writer loadOutputWriter(File file) {
        try {
            OutputStream in = new FileOutputStream(file);
            Writer writer = new OutputStreamWriter(in, Charset.defaultCharset());
            return new BufferedWriter(writer);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
